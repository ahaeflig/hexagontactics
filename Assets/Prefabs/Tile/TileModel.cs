﻿using UnityEngine;
using System.Collections;
using System;

public class TileModel : MonoBehaviour {

    private GameObject tileModel;

    public void InstantiateTileFromName(string path) {
        GameObject model = Resources.Load("Tiles/"+path) as GameObject;
        try { 
            GameObject instance = Instantiate(model,  new Vector3 (transform.parent.transform.position.x, transform.parent.transform.position.y, transform.parent.transform.position.z), transform.parent.transform.rotation) as GameObject;
            instance.transform.parent = this.transform;
            tileModel = instance;
            GetComponentInParent<Tile>().meshRenderer = instance.GetComponent<MeshRenderer>();
            GetComponentInParent<Tile>().defaultTexture = instance.GetComponent<MeshRenderer>().material;
        } catch (ArgumentException) {
            Debug.Log("Wrong path to ressource tile");
        }
    }

    public void UpdateTileFromName(string path) {
        DestroyImmediate(tileModel);
        InstantiateTileFromName(path);
    }



}
