﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour {

    /* AXIAL COORDINATE */
    public Point pos;

    /* CUBE COORDINATE */
    public Cube cube;

    [HideInInspector]
    public Tile prev;
    [HideInInspector]
    public int distance;

    [HideInInspector]
    public GameObject content;

    public Material moveableTexture;
    public Material selectedTexture;
    public Material defaultTexture;

    public MeshRenderer meshRenderer;
    
	void Start () {
    }

    public Vector3 center { get { return new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z); } }

    public void SetTilePosition(int col, int row) {
        pos.x = col;
        pos.y = row;

        cube.x = col;
        cube.z = row - (col + (col & 1)) / 2;
        cube.y = -cube.x - cube.z;
    }

   public void Update() {
    }

    public void Select() {
        if (meshRenderer)
            meshRenderer.material = selectedTexture;
    }

    public void Select2() {
        if (meshRenderer)
            meshRenderer.material = moveableTexture;
    }

    public void Unselect() {
        if (meshRenderer)
            meshRenderer.material = defaultTexture;
    }

    public GameObject getContent() {
        return content;
    }

    #region Preprod

    public void Match() {
        float hshift = (pos.x % 2 != 0) ? 0 : Board.HEIGHT_SHIFT / 2;
        transform.localPosition = new Vector3(pos.x * Board.WIDTH_SHIFT , hshift + pos.y * Board.HEIGHT_SHIFT, 0);
    }

    #endregion


}
