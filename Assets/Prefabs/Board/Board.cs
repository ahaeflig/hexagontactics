﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Board : MonoBehaviour {

    public int gridHeight;
    public int gridWidth;

    public int gridMinHeight = int.MaxValue;
    public int gridMinWidth = int.MaxValue;

    [SerializeField]
    public GameObject tilePrefab;

    public static float HEIGHT_SHIFT = 2 * 0.885f;


    public static float WIDTH_SHIFT = 1.532f;

    public LevelData level;

    private GameObject tileParent;

    Point[] directionEven = new Point[] { new Point(0, -1), new Point(1, 0), new Point(1, 1),
                        new Point(0, 1),  new Point(-1, 1), new Point(-1, 0)};

   Point[] directionOdd = new Point[] { new Point(0, -1), new Point(1, -1), new Point(1, 0),
                        new Point(0, 1),  new Point(-1, 0), new Point(-1, -1)};

    //North, NorthEst, SouthEast, ... But it should work to do directions[Direction.NORTH] to get the North direction.
    /*Cube[] directions = new Cube[] {
         new Cube(+1, -1, 0), new Cube(+1, 0, -1), new Cube(0, +1, -1),
         new Cube(-1, +1, 0), new Cube(-1, 0, +1), new Cube(0, -1, +1)
    };*/

    Cube[] directions = new Cube[] {
         new Cube(-1, +1, 0), new Cube(-1, 0, +1), new Cube(0, -1, +1),
         new Cube(+1, -1, 0), new Cube(+1, 0, -1), new Cube(0, +1, -1)
    };

    public Dictionary<Point, Tile> tiles = new Dictionary<Point, Tile>();

    void Start() {
        tileParent = GameObject.Find("Tiles");
    }

    public Cube cube_direction(int direction) { 
        return directions[direction];
    }
    
    public Tile cube_neighbor(Cube hex, int direction) {
        Cube cube = cube_direction(direction) + hex;
        Tile tile = CubeToHex(cube);
        return tile;
    }

    public Tile offset_neighbor(Tile hex, int direction) {
        Point dir;
        if ((hex.pos.x & 1) == 0) {
            dir = directionOdd[direction];
        } else {
            dir = directionEven[direction];
        }

        return tiles[new Point(hex.pos.x + dir.x, dir.y + hex.pos.y)];
    }

    public void Load() {
        if (level == null)
            return;

        gridHeight = level.boardHeight;
        gridWidth = level.boardWidth;
        int i = 0;
        foreach (Point p in level.tiles) {
            GameObject instance = Instantiate(tilePrefab) as GameObject;
            instance.transform.parent = tileParent.transform;
            instance.name = p.x + "/" + p.y;
            Tile t = instance.GetComponent<Tile>();
            TileModel tm = instance.GetComponentInChildren<TileModel>();
            tm.InstantiateTileFromName(level.tileModel[i]);

            if (level.tileBlocking[i]) {
                t.content = new GameObject();
            }

            i++;
            tiles.Add(new Point(p.x, p.y), t);
            t.SetTilePosition(p.x, p.y);

            t.Match();

            gridHeight = Mathf.Max(gridHeight, p.y);
            gridWidth = Mathf.Max(gridWidth, p.x);
            gridMinHeight = Mathf.Min(gridMinHeight, p.y);
            gridMinWidth = Mathf.Min(gridMinWidth, p.x);
        }
    }

    private Tile CubeToHex(Cube cube) {
        int col = cube.x;
        int row = cube.z + (cube.x + (cube.x & 1)) / 2;
        if (col < gridWidth && row >= 0 && col >= 0 && row < gridHeight) {
            return GetTile(new Point(col, row)); 
        }
        return null;
    }

    private int cube_distance(Tile a, Tile b) {
        return (Mathf.Abs((a.cube.x - b.cube.x)) + Mathf.Abs((a.cube.y - b.cube.y)) + Mathf.Abs((a.cube.z - b.cube.z))) / 2;
    }

    public Tile GetTile(Point p) {
        Tile t = tiles.TryGetValue(p, out t) ? t : null;
        return t;
    }

    public void SelectTiles(List<Tile> tiles) {
        for (int i = tiles.Count - 1; i >= 0; --i)
            tiles[i].Select2();
    }

    public void DeSelectTiles(List<Tile> tiles) {
        for (int i = tiles.Count - 1; i >= 0; --i)
            tiles[i].Unselect();
    }


    public List<Tile> Search(Tile start, Func<Tile, Tile, bool> addTile) {
        List<Tile> retValue = new List<Tile>();
        retValue.Add(start);

        ClearSearch();
        Queue<Tile> checkNext = new Queue<Tile>();
        Queue<Tile> checkNow = new Queue<Tile>();

        start.distance = 0;
        checkNow.Enqueue(start);

        while (checkNow.Count > 0) {
            Tile t = checkNow.Dequeue();
            for (int dir = 0; dir < 6; ++dir) {
                Tile next = cube_neighbor(t.cube, dir);
                if (next == null || next.distance <= t.distance + 1)
                    continue;
                if (addTile(t, next)) {
                    next.distance = t.distance + 1;
                    next.prev = t;
                    checkNext.Enqueue(next);
                    retValue.Add(next);
                }
            }

            if (checkNow.Count == 0)
                SwapReference(ref checkNow, ref checkNext);
        }

        return retValue;
    }

    void SwapReference(ref Queue<Tile> a, ref Queue<Tile> b) {
        Queue<Tile> temp = a;
        a = b;
        b = temp;
    }

    void ClearSearch() {
        foreach (Tile t in tiles.Values) {
            t.prev = null;
            t.distance = int.MaxValue;
        }
    }

    public List<Tile> getTileInLine(Point startPos, Directions dir, int range) {

        Cube cube = startPos.toCube();
        List<Tile> line = new List<Tile>();
        List<Tile> tilesFromDict = new List<Tile>();

        foreach (Tile t in tiles.Values)
            tilesFromDict.Add(t);


        switch (dir) {
            case Directions.North:
                line.AddRange(tilesFromDict.FindAll(tile => tile.cube.x == cube.x && tile.cube.z > cube.z && tile.cube.z <= (cube.z + range) ));
                break;
            case Directions.South:
                line.AddRange(tilesFromDict.FindAll(tile => tile.cube.x == cube.x && tile.cube.z < cube.z && tile.cube.z >= (cube.z - range) ));
                break;
            case Directions.NorthEast:
                line.AddRange(tilesFromDict.FindAll(tile => tile.cube.z == cube.z && tile.cube.x > cube.x && tile.cube.x <= (cube.x + range)));
                break;
            case Directions.SouthWest:
                line.AddRange(tilesFromDict.FindAll(tile => tile.cube.z == cube.z && tile.cube.x < cube.x && tile.cube.x >= (cube.x - range)));
                break;
            case Directions.SouthEast:
                line.AddRange(tilesFromDict.FindAll(tile => tile.cube.y == cube.y && tile.cube.z < cube.z && tile.cube.z >= (cube.z - range)));
                break;
            case Directions.NorthWest:
                line.AddRange(tilesFromDict.FindAll(tile => tile.cube.y == cube.y && tile.cube.z > cube.z && tile.cube.z <= (cube.z + range)));
                break;

            default:
                break;
        }

        return line;
    }

    

}
