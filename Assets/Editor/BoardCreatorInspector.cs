﻿using UnityEngine;
using System.Collections;
using UnityEditor;



[CustomEditor(typeof(BoardCreator))]
public class BoardCreatorInspector : Editor {

    public BoardCreator current
    {
        get
        {
            return (BoardCreator)target;
        }
    }

     public override void OnInspectorGUI() {
        DrawDefaultInspector();

        if (GUILayout.Button("Clear"))
            current.Clear();
        if (GUILayout.Button("Fill"))
            current.Fill();
        if (GUILayout.Button("Delete Here"))
            current.DestroyAtPoint();
        if (GUILayout.Button("Create Here"))
            current.CreateAtPoint();
        if (GUILayout.Button("Validate Tile Change"))
            current.ValidateTileChange();
        if (GUILayout.Button("Save"))
            current.Save();
        if (GUILayout.Button("Load"))
            current.Load();

        if (GUI.changed)
            current.UpdateMarker();
    }
}
