﻿
[System.Serializable]
public struct Cube {

    public int x;
    public int y;
    public int z;

    public Cube(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public static Cube operator +(Cube a, Cube b) {
        return new Cube(a.x + b.x, a.y + b.y, a.z + b.z);
    }

}
