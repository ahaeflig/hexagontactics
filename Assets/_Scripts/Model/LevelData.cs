﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelData : ScriptableObject {
    public int boardWidth;
    public int boardHeight;

    public List<Point> tiles;
    public List<string> tileModel = new List <string>();
    public List<bool> tileBlocking = new List<bool>();

}