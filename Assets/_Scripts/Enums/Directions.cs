﻿using UnityEngine;
using System.Collections;

public enum Directions {
    North,
    NorthEast,
    SouthEast,
    South,
    SouthWest,
    NorthWest
}
