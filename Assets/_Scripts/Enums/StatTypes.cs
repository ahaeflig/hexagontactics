﻿
using UnityEngine;
using System.Collections;

public enum StatTypes {
    LVL, // Level
    EXP, // Experience
    WEIGHT, // Weight determines how much equipment is carried, the maximum amount that can be carried is LVL
    HP,  // Hit Points
    MHP, // Max Hit Points
    ATK, // Physical Attack
    DEF, // Physical Defense
    EVD, // Evade
    RES, // Status Resistance
    SPD, // Speed
    MOV, // Move Range
    CTR, // Counter for turn order
    Count
}