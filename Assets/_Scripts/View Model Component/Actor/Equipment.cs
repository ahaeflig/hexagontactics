﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class Equipment : MonoBehaviour {
    #region Notifications
    public const string EquippedNotification = "Equipment.EquippedNotification";
    public const string UnEquippedNotification = "Equipment.UnEquippedNotification";
    #endregion

    #region Fields / Properties
    public IList<Equippable> items { get { return _items.AsReadOnly(); } }
    public int weight { get; protected set; }
    List<Equippable> _items = new List<Equippable>();
    #endregion

    #region Public
    public void Equip(Equippable item) {
        Stats stats = GetComponent<Stats>();

        //Hardcheck but item with too big weight shouldn't be equippable from the GUI.
        int weightLeft = stats[StatTypes.LVL] - stats[StatTypes.WEIGHT];
        if (weightLeft < weight) {
            return;
        }
        
        UnEquip(item);

        _items.Add(item);
        item.transform.SetParent(transform);
        item.OnEquip();

        this.PostNotification(EquippedNotification, item);
    }

    public void UnEquip(Equippable item) {
        item.OnUnEquip();
        item.transform.SetParent(transform);
        _items.Remove(item);

        this.PostNotification(UnEquippedNotification, item);
    }
    #endregion
}