﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LineAbilityRange : AbilityRange {
    public override bool directionOriented { get { return true; } }

    public override List<Tile> GetTilesInRange(Board board) {
        Point startPos = unit.tile.pos;
        List<Tile> retValue = board.getTileInLine(startPos, unit.dir, horizontal);

        return retValue;
    }


}