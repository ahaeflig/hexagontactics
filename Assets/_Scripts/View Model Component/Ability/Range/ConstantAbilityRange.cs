﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ConstantAbilityRange : AbilityRange {
    public override List<Tile> GetTilesInRange(Board board) {
        // return board.getTilesForAbility(unit.tile, horizontal);
        return board.Search(unit.tile, ExpandSearch);
    }

    bool ExpandSearch(Tile from, Tile to) {
        return (from.distance + 1) <= horizontal;
    }

}