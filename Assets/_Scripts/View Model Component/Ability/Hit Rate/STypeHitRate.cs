﻿using UnityEngine;
using System.Collections;

public class STypeHitRate : HitRate {

    const int SIDE_BONUS_HITRATE = 10;
    const int BACK_BONUS_HITRATE = 20;

    const int RES_CLAMP_MAX = 100;
    const int RES_CLAMP_MIN = 0;


    public override int Calculate(Tile target) {

        Unit defender = target.content.GetComponent<Unit>();

        if (AutomaticMiss(defender))
            return Final(100);

        if (AutomaticHit(defender))
            return Final(0);

        int res = GetResistance(defender);
        res = AdjustForStatusEffects(defender, res);
        res = AdjustForRelativeFacing(defender, res);
        res = Mathf.Clamp(res, RES_CLAMP_MIN, RES_CLAMP_MAX);
        return Final(res);
    }

    int GetResistance(Unit target) {
        Stats s = target.GetComponentInParent<Stats>();
        return s[StatTypes.RES];
    }

    int AdjustForRelativeFacing(Unit target, int rate) {
        switch (attacker.GetFacing(target)) {
            case Facings.Front:
                return rate;
            case Facings.Side:
                return rate - SIDE_BONUS_HITRATE;
            default:
                return rate - BACK_BONUS_HITRATE;
        }
    }
}