﻿using UnityEngine;
using System.Collections;

public class ATypeHitRate : HitRate {

    const float SIDE_BONUS_HITRATE = 2;
    const float BACK_BONUS_HITRATE = 4;

    const int EVD_CLAMP_MAX = 100;
    const int EVD_CLAMP_MIN = 10;

    public override int Calculate(Tile target) {
        Unit defender = target.content.GetComponent<Unit>();

        if (AutomaticHit(defender))
            return Final(0);

        if (AutomaticMiss(defender))
            return Final(100);

        float evade = GetEvade(defender);
        evade = AdjustForRelativeFacing(defender, evade);
        evade = AdjustForStatusEffects(defender, (int)evade);
        evade = Mathf.Clamp(evade, EVD_CLAMP_MIN, EVD_CLAMP_MAX); 
        return Final(evade);
    }

    float GetEvade(Unit target) {
        Stats s = target.GetComponentInParent<Stats>();
        return Mathf.Clamp(s[StatTypes.EVD], 0, 100);
    }

    float AdjustForRelativeFacing(Unit target, float rate) {
        switch (attacker.GetFacing(target)) {
            case Facings.Front:
                return rate;
            case Facings.Side:
                return rate / SIDE_BONUS_HITRATE;
            default:
                return rate / BACK_BONUS_HITRATE;
        }
    }
}