﻿using UnityEngine;
using System.Collections;

public static class PointsExtensions {

   public static Cube toCube(this Point p) {
        Cube cube = new Cube();
        int col = p.x;
        int row = p.y;

        cube.x = col;
        cube.z = row - (col + (col & 1)) / 2;
        cube.y = -cube.x - cube.z;

        return cube;
    }

}
