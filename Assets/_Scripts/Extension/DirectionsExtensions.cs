﻿using UnityEngine;
using System.Collections;

public static class DirectionsExtensions {

    private static int currSelect;

    public static Directions GetDirection(this Tile t1, Tile t2) {
        if (t1.cube.x == t2.cube.x) {
            if (t1.cube.y < t2.cube.y) {
                return Directions.South;
            } else 
                return Directions.North;
        } else if (t1.cube.z == t2.cube.z) {
            if (t2.cube.x > t1.cube.x) 
                return Directions.NorthEast;
            else 
                return Directions.SouthWest;
        } else if (t1.cube.y == t2.cube.y) {
            if (t2.cube.z > t1.cube.z) {
                return Directions.NorthWest;
            } else 
                return Directions.SouthEast;
        } else {
            if (t1.pos.y > t2.pos.y)
                return Directions.South; 
            else
                return Directions.North;
        }
    }

    public static Vector3 ToEuler(this Directions d) {
        return new Vector3(0, 0, nfmod(-1 * (int)d * 60, 360));
    }


    public static void setCurrSelect(this Directions d) {
        currSelect = (int)d;
    }

    //TODO IMPLEMENT DIRECTION FOR HEX TILES
    public static Directions GetDirection(this Point p) {
        if (p.y > 0 || p.x > 0)
            currSelect = (int)nfmod ((currSelect + 1) , 6);
        else
            currSelect = (int)nfmod((currSelect - 1) , 6);

        return (Directions)currSelect;
    }

    private static float nfmod(float a, float b) {
        return a - b * Mathf.Floor(a / b);
    }

    public static Point GetNormal(this Directions dir) {
        switch (dir) {
            case Directions.North:
                return new Point(0, 1);
            case Directions.NorthEast:
                return new Point(1, 1);
            case Directions.SouthEast:
                return new Point(1, -1);
            case Directions.SouthWest:
                return new Point(-1, -1);
            case Directions.NorthWest:
                return new Point(-1, 1);
            default: // Directions.South:
                return new Point(0, -1);
        }
    }

}