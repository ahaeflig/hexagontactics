﻿using UnityEngine;
using System.Collections;

public static class FacingsExtensions {
    public static Facings GetFacing(this Unit attacker, Unit target) {
        Vector2 targetDirection = target.dir.GetNormal();
        Vector2 approachDirection = ((Vector2)(target.tile.center - attacker.tile.center)).normalized;
        float dot = Vector2.Dot(approachDirection, targetDirection);

        //Debug.Log(dot + " Dot");

        if (dot >= 0.6f)
            return Facings.Back;
        if (dot <= -0.6f)
            return Facings.Front;
        return Facings.Side;
    }

}