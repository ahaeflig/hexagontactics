﻿using UnityEngine;
using System.Collections;

public static class CubesExtensions {

    public static Point toPoint(this Cube c) {
        Point point = new Point();

         point.x = c.x;
         point.y = c.z + (c.x + (c.x & 1)) / 2;

        return point;
    }

}
