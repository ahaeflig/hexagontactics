﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.IO;

public class BoardCreator : MonoBehaviour {

    [SerializeField]
    GameObject tileViewPrefab;
    [SerializeField]
    GameObject tileSelector;

    [SerializeField]
    LevelData levelData;

    Dictionary<Point, Tile> tiles = new Dictionary<Point, Tile>();
    Dictionary<Point, string> tileModel = new Dictionary<Point, string>();
    Dictionary<Point, bool> tileBlocking = new Dictionary<Point, bool>();

    [SerializeField]
    int width = 10;
    [SerializeField]
    int height = 8;

    Point pos;

    [Range (0,35)]
    [ExecuteInEditMode]
    public int posX;
    [Range(0, 35)]
    [ExecuteInEditMode]
    public int posY;

    public bool currTileBlocking;
    public string currTileModel;

    void OnValidate() {
        posX = Mathf.Clamp(posX, 0, width - 1);
        posY = Mathf.Clamp(posY, 0, height - 1);
        pos.x = posX;
        pos.y = posY;

    }

    public void ValidateTileChange() {
        if (tileModel[pos] != currTileModel || tileBlocking[pos] != currTileBlocking)
            UpdateTile();
    }

    private void UpdateTile() {
        tileModel[pos] = currTileModel;
        tileBlocking[pos] = currTileBlocking;

        Tile t = tiles[pos];
        t.GetComponentInChildren<TileModel>().UpdateTileFromName(currTileModel);
    }

    public void Fill() {
        Clear();
        for (int i = 0; i < width; ++i) {
            for (int j = 0; j < height; j++) {
                pos = new Point(i,j);
                CreateAtPoint();
            }
        }
    }

    public void UpdateMarker() {
        if (tiles.ContainsKey(pos)) { 
            tileSelector.transform.position = tiles[pos].transform.position;
        }
    }

    public void CreateAtPoint() {
        GameObject instance = Instantiate(tileViewPrefab) as GameObject;
        instance.transform.parent = transform;

        Tile t = instance.GetComponent<Tile>();
        TileModel tm  = t.GetComponentInChildren<TileModel>();
        tm.InstantiateTileFromName("Normal");
        t.pos = pos;
        t.Match();

        tileModel.Add(pos, "Normal");
        tileBlocking.Add(pos, false);
        tiles.Add(pos, t);
    }

    public void CreateFromData() {
        GameObject instance = Instantiate(tileViewPrefab) as GameObject;
        instance.transform.parent = transform;

        Tile t = instance.GetComponent<Tile>();
        TileModel tm = t.GetComponentInChildren<TileModel>();
        tm.InstantiateTileFromName(tileModel[pos]);

        t.pos = pos;
        t.Match();

        tiles.Add(pos, t);
    }


    public void DestroyAtPoint() {
        Tile t = tiles[pos];

        tiles.Remove(pos);
        tileBlocking.Remove(pos);
        tileModel.Remove(pos);

        GameObject obj = t.GetComponent<Transform>().gameObject;
        DestroyImmediate(obj);
    }

    public void Clear() {
        for (int i = transform.childCount - 1; i >= 0; --i)
            DestroyImmediate(transform.GetChild(i).gameObject);

        tiles.Clear();
        tileModel.Clear();
        tileBlocking.Clear();
    }


    public void Save() {
        string filePath = Application.dataPath + "/Resources/Levels";
        if (!Directory.Exists(filePath))
            CreateSaveDirectory();

        LevelData board = ScriptableObject.CreateInstance<LevelData>();
        board.tiles = new List<Point>(tiles.Count);
        board.boardWidth = width;
        board.boardHeight = height;

        foreach (Tile t in tiles.Values) { 
            board.tiles.Add(new Point(t.pos.x, t.pos.y));
            board.tileBlocking.Add(tileBlocking[new Point(t.pos.x, t.pos.y)]);
            board.tileModel.Add(tileModel[new Point(t.pos.x, t.pos.y)]);
        }

        string fileName = string.Format("Assets/Resources/Levels/{1}.asset", filePath, name);
        AssetDatabase.CreateAsset(board, fileName);
    }

    void CreateSaveDirectory() {
        string filePath = Application.dataPath + "/Resources";
        if (!Directory.Exists(filePath))
            AssetDatabase.CreateFolder("Assets", "Resources");
        filePath += "/Levels";
        if (!Directory.Exists(filePath))
            AssetDatabase.CreateFolder("Assets/Resources", "Levels");
        AssetDatabase.Refresh();
    }

    public void Load() {
        Clear();
        if (levelData == null)
            return;

        int i = 0;
        foreach (Point p in levelData.tiles) {
            tileBlocking.Add(p, levelData.tileBlocking[i]);
            tileModel.Add(p, levelData.tileModel[i]);
            pos = p;
            i++;
            CreateFromData();
        }
    }

}