﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MoveSequenceState : BattleState {

    public override void Enter() {
        base.Enter();
        StartCoroutine("Sequence");
    }

    IEnumerator Sequence() {
        //returning 0 will make it wait 1 frame
        yield return 0;
        Movement m = owner.turn.actor.GetComponent<Movement>();
        yield return StartCoroutine(m.Traverse(owner.currentTile));
        turn.hasUnitMoved = true;
        owner.ChangeState<CommandSelectionState>();
    }

}