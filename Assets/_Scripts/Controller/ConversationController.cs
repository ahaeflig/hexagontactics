﻿using UnityEngine;
using System;
using System.Collections;

public class ConversationController : MonoBehaviour {
    const string SHOW_TOP = "Show Top";
    const string SHOW_BOTTOM = "Show Bottom";
    const string HIDE_TOP = "Hide Top";
    const string HIDE_BOTTOM = "Hide Bottom";

    public static event EventHandler completeEvent;

    [SerializeField]
    ConversationPanel leftPanel;
    [SerializeField]
    ConversationPanel rightPanel;
    Canvas canvas;
    IEnumerator conversation;
    Tweener transition;

    void Start() {
        canvas = GetComponentInChildren<Canvas>();
        if (leftPanel.panel.CurrentPosition == null)
            leftPanel.panel.SetPosition(HIDE_BOTTOM, false);
        if (rightPanel.panel.CurrentPosition == null)
            rightPanel.panel.SetPosition(HIDE_BOTTOM, false);
        canvas.gameObject.SetActive(false);
    }

    public void Show(ConversationData data) {
        canvas.gameObject.SetActive(true);
        conversation = Sequence(data);
        conversation.MoveNext();
    }

    public void Next() {
        if (conversation == null || transition != null)
            return;

        conversation.MoveNext();
    }

    IEnumerator Sequence(ConversationData data) {
        for (int i = 0; i < data.list.Count; ++i) {
            SpeakerData sd = data.list[i];

            ConversationPanel currentPanel = (sd.anchor == TextAnchor.UpperLeft || sd.anchor == TextAnchor.MiddleLeft || sd.anchor == TextAnchor.LowerLeft) ? leftPanel : rightPanel;
            IEnumerator presenter = currentPanel.Display(sd);
            presenter.MoveNext();

            string show, hide;
            if (sd.anchor == TextAnchor.UpperLeft || sd.anchor == TextAnchor.UpperCenter || sd.anchor == TextAnchor.UpperRight) {
                show = SHOW_TOP;
                hide = HIDE_TOP;
            }
            else {
                show = SHOW_BOTTOM;
                hide = HIDE_BOTTOM;
            }

            currentPanel.panel.SetPosition(hide, false);
            MovePanel(currentPanel, show);

            yield return null;
            while (presenter.MoveNext())
                yield return null;

            MovePanel(currentPanel, hide);
            transition.completedEvent += delegate (object sender, EventArgs e) {
                conversation.MoveNext();
            };

            yield return null;
        }

        canvas.gameObject.SetActive(false);
        if (completeEvent != null)
            completeEvent(this, EventArgs.Empty);
    }

    void MovePanel(ConversationPanel obj, string pos) {
        transition = obj.panel.SetPosition(pos, true);
        transition.duration = 0.5f;
        transition.equation = EasingEquations.EaseOutQuad;
    }

}