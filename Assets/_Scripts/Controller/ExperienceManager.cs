﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public static class ExperienceManager {
    
    public static void AwardExperience(int amount, Unit actor) {

        if (amount < 0)
            return;
        if (!actor)
            return;

        Rank rank = actor.GetComponent<Rank>();

        rank.EXP += amount;        
    }    

}